import React from 'react';
import './Alert.css';

const Alert = (props) => {

  return(
    <div className={["Alert", props.type].join(' ')} style={{display: props.closed ? "show" : "none"}}
         onClick={props.clickDismissable ? props.dismiss : null} key={props.type}>
      {!props.clickDismissable ? <i className="material-icons Alert-close" onClick={props.dismiss}>clear</i> : null}
      {props.children}
    </div>
  )
};

export default Alert;