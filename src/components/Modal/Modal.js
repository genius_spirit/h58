import React, {Fragment} from 'react';
import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";
import {RaisedButton} from "material-ui";

const Modal = props => {

  return (
    <Fragment>
      <Backdrop show={props.show}/>
      <div className="Modal"
           style={{ transform: props.show ? 'scale(1)' : 'scale(0)',
                    opacity: props.show ? '1' : '0' }}>
        <div className='Modal-title'>
          {props.title}
          <i className="material-icons Modal-close" onClick={props.closed}>clear</i>
        </div>
        {props.children}
        {
          props.buttons.map(item =>
            <RaisedButton
              key={item.type}
              labelColor='#fff'
              label={item.label}
              backgroundColor={item.color}
              style={{margin: 12}}
              onClick={item.clicked}
            />
          )
        }
      </div>
    </Fragment>
  );
};

export default Modal;
