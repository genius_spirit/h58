import React, {Component, Fragment} from 'react';
import Modal from './components/Modal/Modal';
import './App.css';
import RaisedButton from 'material-ui/RaisedButton';
import Alert from "./components/Alert/Alert";


class App extends Component {

  state = {
    modalShow: false,
    alertShow: true,
  };

  buttons = [
    {type: 'primary', color: '#2196F3', label: 'Continue', clicked: () => this.continueHandler()},
    {type: 'danger', color: '#E91E63', label: 'Close', clicked: () => this.closeHandler()}
  ];

  showHandler = () => {
    this.setState({modalShow: true});
  };

  closeHandler = () => {
    this.setState({modalShow: false});
  };

  continueHandler = () => {
    alert(11111111);
  };

  handlerCloseAlert = () => {
    this.setState({alertShow: false});
  };

  render() {
    return (
      <Fragment>
        <Modal
          buttons={this.buttons}
          show={this.state.modalShow}
          closed={this.closeHandler}
          title="Some modal title">
          <p>This is modal content</p>
        </Modal>
        <div className="App">
          <RaisedButton label="Open Modal" onClick={this.showHandler}/>
        </div>
        <Alert dismiss={this.handlerCloseAlert}
               clickDismissable
               type="warning"
               closed={this.state.alertShow}>
          <div>This is a warning type alert</div>
        </Alert>
        <Alert dismiss={this.handlerCloseAlert}
               type="primary"
               closed={this.state.alertShow}>
          <div>This is a primary type alert</div>
        </Alert>
        <Alert dismiss={this.handlerCloseAlert}
               clickDismissable
               type="success"
               closed={this.state.alertShow}>
          <div>This is a success type alert</div>
        </Alert>
       </Fragment>
    );
  }
}

export default App;
